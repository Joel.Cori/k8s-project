# Online Boutique Deployment

ubuntu@10.195.71.93

## Getting Started

### Step 1: Clone the Repository

Clone the Online Boutique repository to your local machine.

```sh
git clone https://gitlab.unige.ch/oct_cui/k8s-project.git
cd k8s-project
```


### (Step 2: Pull the Docker Images)

Make the script executable by running:

```sh
chmod +x pull_images.sh
```
Run the Script

```sh
./pull_images.sh
```



### Step 3: Build Docker Images

Build the Docker images for each microservice. You can use the following commands:

```sh
cd src/adservice
sudo docker build -t adservice:latest .
cd ../cartservice
sudo docker build -t cartservice:latest .
cd ../checkoutservice
sudo docker build -t checkoutservice:latest .
cd ../currencyservice
sudo docker build -t currencyservice:latest .
cd ../emailservice
sudo docker build -t emailservice:latest .
cd ../frontend
sudo docker build -t frontend:latest .
cd ../paymentservice
sudo docker build -t paymentservice:latest .
cd ../productcatalogservice
sudo docker build -t productcatalogservice:latest .
cd ../recommendationservice
sudo docker build -t recommendationservice:latest .
cd ../shippingservice
sudo docker build -t shippingservice:latest .
```

### Step 3: Check the Kubernetes Deployment with Helm

Ensure your Helm chart is set up correctly. The directory structure should look like this:

```
online-boutique/
  ├── charts/
  ├── templates/
      ├── deployment.yaml
      ├── frontend-deployment.yaml         
      ├── frontend-service.yaml      
      ├── hpa.yaml
      ├── ingress.yaml
      ├── NOTES.txt
      ├── service.yaml
      ├── serviceaccount.yaml
  ├── values.yaml
  └── Chart.yaml
```

### Step 4: Deploy the Helm Chart

Deploy the Online Boutique Helm chart:

```sh
microk8s helm3 upgrade --install online-boutique ./online-boutique
```

### Step 5: Verify Deployment

Verify that all pods and services are running correctly:

```sh
microk8s kubectl get pods
microk8s kubectl get svc
```

### Step 6: Port Forward the Frontend Service

Port forward the frontend service to access the application:

```sh
microk8s kubectl port-forward svc/frontend 8080:8080
```

### Step 7: Access the Application

Open your browser and navigate to:

```
http://localhost:8080
```

You should see the Online Boutique application running. You can now interact with the app, browse products, add items to the cart, and checkout.

## Troubleshooting

If you encounter any issues, check the logs of the respective pods:

```sh
microk8s kubectl logs <pod-name>
```

For more detailed debugging, you can describe the resources:

```sh
microk8s kubectl describe pod <pod-name>
microk8s kubectl describe svc <svc-name>
```

## Cleanup

To remove the deployment, use the following command:

```sh
microk8s helm3 uninstall online-boutique
```

To stop and remove MicroK8s, use:

```sh
microk8s stop
microk8s reset
```