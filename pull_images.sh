#!/bin/bash

# DockerHub username and project name
REGISTRY="registry.gitlab.unige.ch/joel.cori"

# Array of image names
IMAGES=(
  "shippingservice"
  "productcatalogservice"
  "frontend"
  "currencyservice"
  "cartservice"
  "shoppingassistanservice"
  "recommendationservice"
  "paymentservice"
  "emailservice"
  "checkoutservice"
  "adservice"
)

# Loop through each image and pull it
for IMAGE in "${IMAGES[@]}"; do
  sudo docker pull $REGISTRY/$IMAGE
done